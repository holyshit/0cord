# 0cord

Maintained pitch black CSS for Discord

Works fine as of January 9, 2020, tested on Discord Stable.

Use stylus to install to your browser, and use anything *but* BetterDiscord to install to your client. Keep in mind that client modding is a violation of Discord ToS and can get you banned.

Also on userstyles: https://userstyles.org/styles/163340/0cord-pitch-black-discord

## Screenshot

![A screenshot of discord with this CSS theme](https://elixi.re/i/oisjdgn6.png)

## Use 0cord on your phone!

Oh yeah, if you own an Android phone, you can now switch to a theme based on 0cord on your phone! See [cutthecord repo](https://gitdab.com/distok/cutthecord#user-content-binaries-apk) for more details (the patch "customtheme" is the one used for this)!

![A screenshot of discord android with a black amoled theme applied](https://elixi.re/i/u3hmnng3.png)

## License

CC-BY-NC
